#-------------------------------------------------
#
# Project created by QtCreator 2016-11-25T12:45:13
#
#-------------------------------------------------

QT       += core gui sql printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = CSM
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mymsg.cpp \
    studentsdialog.cpp \
    subjectsdialog.cpp \
    addstudentsdialog.cpp \
    configdialog.cpp \
    aboutdialog.cpp \
    commonsubjectsdialog.cpp

HEADERS  += mainwindow.h \
    mymsg.h \
    studentsdialog.h \
    subjectsdialog.h \
    addstudentsdialog.h \
    configdialog.h \
    aboutdialog.h \
    commonsubjectsdialog.h

FORMS    += mainwindow.ui \
    studentsdialog.ui \
    subjectsdialog.ui \
    addstudentsdialog.ui \
    configdialog.ui \
    aboutdialog.ui \
    commonsubjectsdialog.ui

VERSION = 1.0.0
QMAKE_TARGET_COMPANY = "���� ���������"
QMAKE_TARGET_PRODUCT = "����� ���� ������"
QMAKE_TARGET_DESCRIPTION = "����� ���� ������"
QMAKE_TARGET_COPYRIGHT = "���� ������ ������ � ���� ���������"

RESOURCES += \
    Resources.qrc

RC_ICONS = icons\mainicon.ico
