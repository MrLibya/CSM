#include "addstudentsdialog.h"
#include "ui_addstudentsdialog.h"
#include <QtSql>
#include "mymsg.h"

AddStudentsDialog::AddStudentsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddStudentsDialog)
{
    ui->setupUi(this);
    QSqlQuery query;
    query.exec("SELECT name FROM students");
    while(query.next())
        ui->studentName->addItem(query.value(0).toString());
}

AddStudentsDialog::~AddStudentsDialog()
{
    delete ui;
}

void AddStudentsDialog::on_studentName_currentTextChanged(const QString &arg1)
{
    ui->pushButton_2->setEnabled(false);
    if(ui->studentName->findText(arg1) >= 0)
        ui->pushButton_2->setEnabled(true);
}

void AddStudentsDialog::on_pushButton_2_clicked()
{
    QSqlQuery query;
    query.exec(QString("DELETE FROM students WHERE name = '%1'").arg(ui->studentName->currentText()));
    AddStudentsDialog::close();
    deleteSuccessfullyMsg();
}

void AddStudentsDialog::on_pushButton_clicked()
{
    if(ui->studentName->findText(ui->studentName->currentText()) >= 0){
        studentIsExistMsg();
        return;
    }
    QSqlDatabase::database().transaction();
    QSqlQuery query;
    query.exec(QString("INSERT INTO students (name)" "VALUES ('%1')").arg(ui->studentName->currentText()));
    QSqlDatabase::database().commit();
    AddStudentsDialog::close();
    addSuccessfullyMsg();
}
