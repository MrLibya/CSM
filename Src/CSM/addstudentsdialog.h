#ifndef ADDSTUDENTSDIALOG_H
#define ADDSTUDENTSDIALOG_H

#include <QDialog>

namespace Ui {
class AddStudentsDialog;
}

class AddStudentsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddStudentsDialog(QWidget *parent = 0);
    ~AddStudentsDialog();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_studentName_currentTextChanged(const QString &arg1);

private:
    Ui::AddStudentsDialog *ui;
};

#endif // ADDSTUDENTSDIALOG_H
