#include "commonsubjectsdialog.h"
#include "ui_commonsubjectsdialog.h"
#include "mymsg.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QDebug>

CommonSubjectsDialog::CommonSubjectsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CommonSubjectsDialog)
{
    ui->setupUi(this);
    ui->ClassYear->addItem("الثانية");
    ui->ClassYear->addItem("الثالثة");
    ui->ClassYear->addItem("الرابعة");
    ui->ClassYear->addItem("الخامسة");

    QSqlQuery query("SELECT name FROM common_subjects");
    while(query.next())
        if (ui->subject->findText(query.value(0).toString())  < 0)
            ui->subject->addItem(query.value(0).toString());
}

CommonSubjectsDialog::~CommonSubjectsDialog()
{
    delete ui;
}

void CommonSubjectsDialog::on_Saturday_clicked()
{
    if(ui->Saturday->isChecked()){
        ui->label->setEnabled(true);
        ui->label_11->setEnabled(true);
        ui->from_Saturday->setEnabled(true);
        ui->to_Saturday->setEnabled(true);
    }
    else{
        ui->label->setEnabled(false);
        ui->label_11->setEnabled(false);
        ui->from_Saturday->setEnabled(false);
        ui->to_Saturday->setEnabled(false);
    }
}

void CommonSubjectsDialog::on_Sunday_clicked()
{
    if(ui->Sunday->isChecked()){
        ui->label_2->setEnabled(true);
        ui->label_12->setEnabled(true);
        ui->from_Sunday->setEnabled(true);
        ui->to_Sunday->setEnabled(true);
    }
    else{
        ui->label_2->setEnabled(false);
        ui->label_12->setEnabled(false);
        ui->from_Sunday->setEnabled(false);
        ui->to_Sunday->setEnabled(false);
    }
}

void CommonSubjectsDialog::on_Monday_clicked()
{
    if(ui->Monday->isChecked()){
        ui->label_3->setEnabled(true);
        ui->label_7->setEnabled(true);
        ui->from_Monday->setEnabled(true);
        ui->to_Monday->setEnabled(true);
    }
    else{
        ui->label_3->setEnabled(false);
        ui->label_7->setEnabled(false);
        ui->from_Monday->setEnabled(false);
        ui->to_Monday->setEnabled(false);
    }
}

void CommonSubjectsDialog::on_Tuesday_clicked()
{
    if(ui->Tuesday->isChecked()){
        ui->label_4->setEnabled(true);
        ui->label_8->setEnabled(true);
        ui->from_Tuesday->setEnabled(true);
        ui->to_Tuesday->setEnabled(true);
    }
    else{
        ui->label_4->setEnabled(false);
        ui->label_8->setEnabled(false);
        ui->from_Tuesday->setEnabled(false);
        ui->to_Tuesday->setEnabled(false);
    }
}

void CommonSubjectsDialog::on_Wednesday_clicked()
{
    if(ui->Wednesday->isChecked()){
        ui->label_6->setEnabled(true);
        ui->label_9->setEnabled(true);
        ui->from_Wednesday->setEnabled(true);
        ui->to_Wednesday->setEnabled(true);
    }
    else{
        ui->label_6->setEnabled(false);
        ui->label_9->setEnabled(false);
        ui->from_Wednesday->setEnabled(false);
        ui->to_Wednesday->setEnabled(false);
    }
}

void CommonSubjectsDialog::on_Thursday_clicked()
{
    if(ui->Thursday->isChecked()){
        ui->label_5->setEnabled(true);
        ui->label_10->setEnabled(true);
        ui->from_Thursday->setEnabled(true);
        ui->to_Thursday->setEnabled(true);
    }
    else{
        ui->label_5->setEnabled(false);
        ui->label_10->setEnabled(false);
        ui->from_Thursday->setEnabled(false);
        ui->to_Thursday->setEnabled(false);
    }
}

void CommonSubjectsDialog::on_pushButton_clicked()
{
    if(ui->professor->text() == "" || ui->subject->currentText() == ""){
        emptyInfoMsg();
        return;
    }
    QSqlDatabase::database().transaction();
    QSqlQuery query;
    if (ui->Saturday->isChecked())
        query.exec(QString("INSERT OR REPLACE INTO common_subjects (id,name, professor, year, day, 'from', 'to')"
                       "VALUES ((select id from common_subjects where name = '%1' AND year = '%3' AND day = 1),'%1','%2','%3',1,'%4','%5')").arg(ui->subject->currentText(),ui->professor->text(),QString::number(ui->ClassYear->currentIndex()+2),ui->from_Saturday->text(),ui->to_Saturday->text()));
    if(ui->Sunday->isChecked())
        query.exec(QString("INSERT OR REPLACE INTO common_subjects (id,name, professor, year, day, 'from', 'to')"
                        "VALUES ((select id from common_subjects where name = '%1' AND year = '%3' AND day = 2),'%1','%2','%3',2,'%4','%5')").arg(ui->subject->currentText(),ui->professor->text(),QString::number(ui->ClassYear->currentIndex()+2),ui->from_Sunday->text(),ui->to_Sunday->text()));
    if(ui->Monday->isChecked())
        query.exec(QString("INSERT OR REPLACE INTO common_subjects (id,name, professor, year, day, 'from', 'to')"
                        "VALUES ((select id from common_subjects where name = '%1' AND year = '%3' AND day = 3),'%1','%2','%3',3,'%4','%5')").arg(ui->subject->currentText(),ui->professor->text(),QString::number(ui->ClassYear->currentIndex()+2),ui->from_Monday->text(),ui->to_Monday->text()));
    if(ui->Tuesday->isChecked())
        query.exec(QString("INSERT OR REPLACE INTO common_subjects (id,name, professor, year, day, 'from', 'to')"
                        "VALUES ((select id from common_subjects where name = '%1' AND year = '%3' AND day = 4),'%1','%2','%3',4,'%4','%5')").arg(ui->subject->currentText(),ui->professor->text(),QString::number(ui->ClassYear->currentIndex()+2),ui->from_Tuesday->text(),ui->to_Tuesday->text()));
    if(ui->Wednesday->isChecked())
        query.exec(QString("INSERT OR REPLACE INTO common_subjects (id,name, professor, year, day, 'from', 'to')"
                        "VALUES ((select id from common_subjects where name = '%1' AND year = '%3' AND day = 5),'%1','%2','%3',5,'%4','%5')").arg(ui->subject->currentText(),ui->professor->text(),QString::number(ui->ClassYear->currentIndex()+2),ui->from_Wednesday->text(),ui->to_Wednesday->text()));
    if(ui->Thursday->isChecked())
        query.exec(QString("INSERT OR REPLACE INTO common_subjects (id,name, professor, year, day, 'from', 'to')"
                        "VALUES ((select id from common_subjects where name = '%1' AND year = '%3' AND day = 6),'%1','%2','%3',6,'%4','%5')").arg(ui->subject->currentText(),ui->professor->text(),QString::number(ui->ClassYear->currentIndex()+2),ui->from_Thursday->text(),ui->to_Thursday->text()));
    QSqlDatabase::database().commit();
    CommonSubjectsDialog::close();
    addSuccessfullyMsg();
}

void CommonSubjectsDialog::on_pushButton_3_clicked()
{
    QSqlQuery query;
    if(ui->subject->currentText() == ""){
        emptyInfoMsg();
        return;
    }
    if (ui->subject->findText(ui->subject->currentText()) < 0){
        subjectIsNotExistMsg();
        return;
    }

    query.exec(QString("DELETE FROM common_subjects WHERE name = '%1'").arg(ui->subject->currentText()));
    deleteSuccessfullyMsg();
    CommonSubjectsDialog::close();
}
