#ifndef COMMONSUBJECTSDIALOG_H
#define COMMONSUBJECTSDIALOG_H

#include <QDialog>

namespace Ui {
class CommonSubjectsDialog;
}

class CommonSubjectsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit CommonSubjectsDialog(QWidget *parent = 0);
    ~CommonSubjectsDialog();

private slots:
    void on_Saturday_clicked();

    void on_Sunday_clicked();

    void on_Monday_clicked();

    void on_Tuesday_clicked();

    void on_Wednesday_clicked();

    void on_Thursday_clicked();

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::CommonSubjectsDialog *ui;
};

#endif // COMMONSUBJECTSDIALOG_H
