#include "configdialog.h"
#include "ui_configdialog.h"
#include <QSqlQuery>
#include "mymsg.h"
#include <QSqlError>
#include <QDebug>

ConfigDialog::ConfigDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfigDialog)
{
    ui->setupUi(this);
    QSqlQuery query;
    query.exec("SELECT * FROM config");
    if(query.first())
    {
        ui->university->setText(query.value(0).toString());
        ui->college->setText(query.value(1).toString());
        ui->department->setText(query.value(2).toString());
    }
    ui->semester->addItem("الأول");
    ui->semester->addItem("الثاني");
}

ConfigDialog::~ConfigDialog()
{
    delete ui;
}

void ConfigDialog::on_pushButton_2_clicked()
{
    if(ui->university->text() == "" || ui->college->text() == "" || ui->department->text() == ""){
        emptyInfoMsg();
        return;
    }
    QSqlQuery query("SELECT * FROM config");
    if (query.first())
        query.exec(QString("UPDATE config SET university = '%1', college = '%2', department = '%3', semester = '%4'").arg(ui->university->text()).arg(ui->college->text()).arg(ui->department->text()).arg(QString::number(ui->semester->currentIndex()+1)));
    else
        query.exec(QString("INSERT INTO config (university, college, department, semester)"
                           "VALUES('%1','%2','%3','%4')").arg(ui->university->text()).arg(ui->college->text()).arg(ui->department->text()).arg(QString::number(ui->semester->currentIndex()+1)));
    qDebug()<<query.lastError().text();
    ConfigDialog::_university = ui->university->text();
    ConfigDialog::_college = ui->college->text();
    ConfigDialog::_department = ui->department->text();
    ConfigDialog::_semester = ui->semester->currentIndex()+1;
    configUpdatedMsg();
    ConfigDialog::close();
}
