#include "mainwindow.h"
#include <QApplication>
#include "mymsg.h"
#include <QSplashScreen>
#include <QTimer>
#include <memory>
#include <QStyleFactory>
#include <QtSql>
#include "configdialog.h"

QString ConfigDialog::_university = "";
QString ConfigDialog::_college = "";
QString ConfigDialog::_department = "";
int     ConfigDialog::_semester = 0;

int main(int argc, char *argv[])
{
    QApplication::setStyle(QStyleFactory::create("Fusion"));
    QApplication a(argc, argv);

    // DB
    QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName(QApplication::applicationDirPath()+ QDir::separator() + "CSM.db");
    if(!QFile::exists(QApplication::applicationDirPath()+ QDir::separator() + "CSM.db") || !db.open())
    {
        DBFailMsg();
        return 1;
    }
    QPixmap pixmap(":/icons/big.ico");
    std::unique_ptr<QSplashScreen> splash(new QSplashScreen(pixmap));
    splash->show();
    QTimer::singleShot(3000, splash.get(), SLOT(hide()));
    QTime later = QTime::currentTime().addSecs(3).addMSecs(300);
    notyet:
    if(QTime::currentTime() < later)
        goto notyet;

    QSqlQuery query;
    query.exec("SELECT * FROM config");
    if (query.first()){
        ConfigDialog::_university = query.value(0).toString();
        ConfigDialog::_college = query.value(1).toString();
        ConfigDialog::_department = query.value(2).toString();
        ConfigDialog::_semester = query.value(3).toInt();
    }
    else{
        ConfigDialog d_configdialog;
        d_configdialog.setModal(true);
        d_configdialog.exec();
    }
    if(!ConfigDialog::_semester)
        return 1;

    MainWindow w;
    w.show();
    splash->finish(&w);
    return a.exec();
}
