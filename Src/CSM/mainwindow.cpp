#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QtSql>
#include <QLabel>
#include <QPrinter>
#include <QTextDocument>
#include "mymsg.h"
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    showMaximized();
    QLabel *text = new QLabel(this);
    QLabel *text2 = new QLabel(this);
    QLabel *text3 = new QLabel(this);
    QLabel *text4 = new QLabel(this);
    text->setText(" جامعة " + ConfigDialog::_university);
    text2->setText(" كلية " + ConfigDialog::_college);
    text3->setText(" قسم " + ConfigDialog::_department);
    if(ConfigDialog::_semester == 1)
        text4->setText("السمستر الاول");
    else
        text4->setText("السمستر الثاني");
    ui->statusBar->addPermanentWidget(text,1);
    ui->statusBar->addPermanentWidget(text2,1);
    ui->statusBar->addPermanentWidget(text3,1);
    ui->statusBar->addPermanentWidget(text4,1);

    makeTheTable();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_AddSubject_triggered()
{
    d_subjectsdialog = new SubjectsDialog(this);
    d_subjectsdialog->setModal(true);
    d_subjectsdialog->exec();
}

void MainWindow::on_AddStudent_triggered()
{
    d_addstudentsdialog = new AddStudentsDialog(this);
    d_addstudentsdialog->setModal(true);
    d_addstudentsdialog->exec();
}

void MainWindow::on_StudentManger_triggered()
{
    d_studentsdialog = new studentsDialog(this);
    d_studentsdialog->setModal(true);
    d_studentsdialog->exec();
}

void MainWindow::on_Config_triggered()
{
    d_configdialog = new ConfigDialog(this);
    d_configdialog->setModal(true);
    d_configdialog->exec();
}


void MainWindow::on_about_triggered()
{
    d_aboutdialog = new AboutDialog(this);
    d_aboutdialog->setModal(true);
    d_aboutdialog->exec();
}

void MainWindow::on_makeTheTable_clicked()
{
    ui->progressBar->setValue(0);
    ui->table->setRowCount(0);
    makeTheTable();
    QSqlQuery query;
    QStringList subjectsAvalibe;
    //const int weekHours = 36;
    float subjectHours=0.0;
    int subjectDays,randDay,randTime,current_day,current_time;
    int days[6] = {-2,2,6,10,14,18};
    int tims[6] = {2,3,4,5,6,7};
    std::vector<int> day;
    std::vector<int> time;
    bool canSetItem;
    query.exec("SELECT * FROM common_subjects");
    while(query.next()){
        int row = 4*(query.value(4).toInt()-1) + query.value(3).toInt() - 2;
        int col = query.value(5).toInt() - 6;
        int span = query.value(6).toInt() - query.value(5).toInt();
        ui->table->setItem(row, col, new QTableWidgetItem(query.value(1).toString() + " ( " + query.value(2).toString() + " ) "));
        if(span > 1){
            ui->table->setSpan(row, col, 1, span);
            ui->table->item(row, col)->setTextAlignment(Qt::AlignCenter);
        }
    }
    query.exec("SELECT * FROM students_subjects WHERE name IN (SELECT name FROM subjects)");
    while(query.next()){
        if(!subjectsAvalibe.contains(query.value(1).toString()))
            subjectsAvalibe << query.value(1).toString();
    }

    query.exec(QString("SELECT * FROM subjects WHERE semester = '%1'").arg(QString::number(ConfigDialog::_semester)));
    query.last();
    int total = query.at() + 1 ;
    query.first();
    do{
        ui->progressBar->setValue(ui->progressBar->value()+100/total);
        if(subjectsAvalibe.contains(query.value(1).toString())){
            qsrand(QTime::currentTime().msec());
            subjectHours = query.value(5).toFloat();
            subjectDays  = query.value(4).toInt();
            day.clear();
            time.clear();
            for(size_t j=0;j<6;j++)
                day.push_back(days[j]);

            newday:
            for(size_t j=0;j<6;j++)
                time.push_back(tims[j]);
            current_day = qrand() % day.size();
            randDay = day[current_day];
            randDay += query.value(3).toInt();
            canSetItem = false;
            while(subjectDays > 0){
                if(time.empty()){
                    if(day.empty()){
                        //tableMakeRandomFaildMsg();
                        on_makeTheTable_clicked();
                        return;
                    }
                    goto newday;
                }
                while(!time.empty()){
                    current_time = qrand() % time.size();
                    randTime = time[current_time];
                    if (randTime + subjectHours > 8 || itemExist(randDay,randTime,subjectHours) || ui->table->columnSpan(randDay,randTime) > 1
                            || ui->table->columnSpan(randDay,randTime+subjectHours-1) > 1 || studentHasCollision(randDay,randTime)
                            || subjectIsExist(randDay,query.value(1).toString())){
                        time.erase(time.begin() + current_time);
                        continue;
                    }
                    else{
                        canSetItem = true;
                        break;
                    }
                }
                if(canSetItem){
                    subjectDays--;
                    ui->table->setItem(randDay,randTime,new QTableWidgetItem(query.value(1).toString() + " ( " + query.value(2).toString() + " ) "));
                    if(subjectHours > 1.0){
                        ui->table->setSpan(randDay,randTime,1,subjectHours);
                        ui->table->item(randDay,randTime)->setTextAlignment(Qt::AlignCenter);
                    }
                day.erase(day.begin() + current_day);
               }
            }
        }
    }while(query.next());
    if(ui->progressBar->value() != 100)
        ui->progressBar->setValue(100);
    /*
    QPrinter printer;
    printer.setPageMargins(QMarginsF(15, 15, 15, 15));
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPageSize(QPrinter::A4);
    printer.printRange();
    printer.setOutputFileName("table.pdf");
    ui->table->render(&printer);

    QPixmap originalPixmap = QPixmap::grabWidget(ui->table);
    QFileDialog dialog(this, Qt::Dialog);
    dialog.setDefaultSuffix("bmp");

    dialog.setAcceptMode(QFileDialog::AcceptSave);
    if (dialog.exec()) {
        QString fileName = dialog.selectedFiles().first();
        originalPixmap.save(fileName, "BMP", 100);
    }

    QString html ="<html><body>"
            "<h2 style=color: #2e6c80; text-align: center;>%1 - %2 - %3</h2>"
    "<table style=height: 212px; width=575>"
    "<tbody>"
    "<tr>"
    "<td>"
    "<h2>1-2</h2>"
    "</td>"
    "<td>"
    "<h2>12-1</h2>"
    "</td>"
    "<td>"
    "<h2>11-12</h2>"
    "</td>"
    "<td>"
    "<h2>10-11</h2>"
    "</td>"
    "<td>"
    "<h2>9-10</h2>"
    "</td>"
    "<td>"
    "<h2>8-9</h2>"
    "</td>"
    "<td>"
    "<h2>السنة</h2>"
    "</td>"
    "<td>"
    "<h2>ايام الأسبوع</h2>"
    "</td>"
    "</tr>"
    "<tr>"
    "<td>&nbsp;</td>"
    "<td>&nbsp;</td>"
    "<td>&nbsp;</td>"
    "<td>&nbsp;</td>"
    "<td>&nbsp;</td>"
    "<td>&nbsp;</td>"
    "<td>&nbsp;</td>"
    "<td>"
    "<h3>السبت</h3>"
    "</td>"
    "</tr>"
    "<tr>"
    "<td>&nbsp;</td>"
    "<td>&nbsp;</td>"
    "<td>&nbsp;</td>"
    "<td>&nbsp;</td>"
    "<td>&nbsp;</td>"
    "<td>&nbsp;</td>"
    "<td>&nbsp;</td>"
    "</tbody>"
    "</table>"
    "</body></html>";
    QTextDocument document;
    document.setHtml(html.arg(ConfigDialog::_university,ConfigDialog::_college,ConfigDialog::_department));
    QPrinter printer;
    printer.setPageMargins(QMarginsF(15, 15, 15, 15));
    printer.setOutputFormat(QPrinter::PdfFormat);
    printer.setPageSize(QPrinter::A4);
    printer.printRange();
    printer.setOutputFileName("table.pdf");
    document.print(&printer);
    */
    /*
    QPainter painter;
    if (!painter.begin(&printer)){ // failed to open file
        makeTableFailedMsg();
        return;
    }
    painter.setLayoutDirection(Qt::RightToLeft);
    painter.end();
    */
}

void MainWindow::on_table_itemSelectionChanged()
{
    qDebug()<<ui->table->currentColumn();
    qDebug()<<ui->table->currentRow();
}

bool MainWindow::studentHasCollision(int row,int col)
{
    int days[6] ={0,4,8,12,16,20};
    int firstRow;
    for(int i=0;i<6;i++){
        if(row >= days[i] && row < days[i+1]){
            firstRow= days[i];
            break;
        }
    }
    QSqlQuery query("SELECT id FROM students");
    QSqlQuery query2;
    while(query.next()){
        query2.exec(QString("SELECT name FROM students_subjects WHERE id = '%1'").arg(query.value(0).toInt()));
        for(int i=firstRow;i<firstRow+4;i++){
            query2.first();
            do{
                if(query2.value(0).toString() == ui->table->item(i,col)->text())
                    return true;
            }while(query2.next());
        }
    }
    return false;
}

void MainWindow::makeTheTable()
{
    ui->table->horizontalHeader()->show();
    ui->table->verticalHeader()->hide();
    ui->table->setRowCount(24);
    ui->table->setItem(0,0,new QTableWidgetItem("السبت"));
    ui->table->setItem(4,0,new QTableWidgetItem("الاحد"));
    ui->table->setItem(8,0,new QTableWidgetItem("الاثنين"));
    ui->table->setItem(12,0,new QTableWidgetItem("الثلاثاء"));
    ui->table->setItem(16,0,new QTableWidgetItem("الاربعاء"));
    ui->table->setItem(20,0,new QTableWidgetItem("الخميس"));
    ui->table->setSpan(0,0,4,1);
    ui->table->setSpan(4,0,4,1);
    ui->table->setSpan(8,0,4,1);
    ui->table->setSpan(12,0,4,1);
    ui->table->setSpan(16,0,4,1);
    ui->table->setSpan(20,0,4,1);

    for(int i=0;i<6;i++){
        ui->table->setItem(i*4,1,new QTableWidgetItem("2"));
        ui->table->setItem(i*4+1,1,new QTableWidgetItem("3"));
        ui->table->setItem(i*4+2,1,new QTableWidgetItem("4"));
        ui->table->setItem(i*4+3,1,new QTableWidgetItem("5"));
    }
    for(int i=0;i<ui->table->rowCount();i++)
    {
        for(int j=2;j<ui->table->columnCount();j++){
            ui->table->setItem(i,j,new QTableWidgetItem(""));
        }
    }
}

bool MainWindow::subjectIsExist(int row,QString subject)
{
    for(int i=2;i<=7;i++){
        if (ui->table->item(row,i)->text() == subject)
            return true;
    }
    return false;
}

void MainWindow::on_CommonSubject_triggered()
{
    d_commonsubjectsdialog = new CommonSubjectsDialog(this);
    d_commonsubjectsdialog->setModal(true);
    d_commonsubjectsdialog->exec();
}

bool MainWindow::itemExist(int row, int col, int colspan)
{
    for(int i = col;i<col+colspan;i++){
        if (ui->table->item(row,i)->text() != "")
            return true;
    }
    return false;
}
