#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <subjectsdialog.h>
#include "addstudentsdialog.h"
#include "studentsdialog.h"
#include "configdialog.h"
#include "aboutdialog.h"
#include "commonsubjectsdialog.h"
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    bool studentHasCollision(int, int);
    void makeTheTable();
    bool subjectIsExist(int,QString);
    bool itemExist(int, int, int);

private slots:
    void on_AddSubject_triggered();

    void on_AddStudent_triggered();

    void on_StudentManger_triggered();

    void on_Config_triggered();

    void on_about_triggered();

    void on_makeTheTable_clicked();

    void on_table_itemSelectionChanged();

    void on_CommonSubject_triggered();

private:
    Ui::MainWindow *ui;
    SubjectsDialog *d_subjectsdialog;
    AddStudentsDialog *d_addstudentsdialog;
    studentsDialog *d_studentsdialog;
    ConfigDialog *d_configdialog;
    AboutDialog *d_aboutdialog;
    CommonSubjectsDialog *d_commonsubjectsdialog;
};

#endif // MAINWINDOW_H
