#include "mymsg.h"
#include <QMessageBox>

void DBFailMsg()
{
    QMessageBox db_msg;

    db_msg.setIcon(QMessageBox::Critical);
    db_msg.setWindowTitle("خطا في الاتصال");
    db_msg.setText("يوجد خطا في الاتصال بقاعده البينات");
    db_msg.setStandardButtons(QMessageBox::Close);
    db_msg.setButtonText(QMessageBox::Close,"انهاء");
    db_msg.exec();
}

bool yesNoMsg(QString title)
{
    QMessageBox messageBox(QMessageBox::Question,title,"هل تريد تاكيد العمليه؟",QMessageBox::Yes|QMessageBox::No);
    messageBox.setButtonText(QMessageBox::Yes, "موافق");
    messageBox.setButtonText(QMessageBox::No,  "الغاء");
    if (messageBox.exec() == QMessageBox::Yes)
        return true;
    return false;
}

void configUpdatedMsg()
{
    QMessageBox db_msg;

    db_msg.setIcon(QMessageBox::Information);
    db_msg.setWindowTitle("تم التحديث");
    db_msg.setText("تم تحديث الاعدادات");
    db_msg.setStandardButtons(QMessageBox::Close);
    db_msg.setButtonText(QMessageBox::Close,"انهاء");
    db_msg.exec();
}

void emptyInfoMsg()
{
    QMessageBox db_msg;

    db_msg.setIcon(QMessageBox::Critical);
    db_msg.setWindowTitle("فشل");
    db_msg.setText("يجب ملئ جميع الخانات");
    db_msg.setStandardButtons(QMessageBox::Close);
    db_msg.setButtonText(QMessageBox::Close,"انهاء");
    db_msg.exec();
}

void addSuccessfullyMsg()
{
    QMessageBox db_msg;

    db_msg.setIcon(QMessageBox::Information);
    db_msg.setWindowTitle("تمت العمليه");
    db_msg.setText("تم الاضافة بنجاح");
    db_msg.setStandardButtons(QMessageBox::Close);
    db_msg.setButtonText(QMessageBox::Close,"انهاء");
    db_msg.exec();
}

void subjectIsExistMsg()
{
    QMessageBox db_msg;

    db_msg.setIcon(QMessageBox::Critical);
    db_msg.setWindowTitle("فشل");
    db_msg.setText("هذه المادة الدراسيه موجوده مسبقا");
    db_msg.setStandardButtons(QMessageBox::Close);
    db_msg.setButtonText(QMessageBox::Close,"انهاء");
    db_msg.exec();
}

void studentIsExistMsg()
{
    QMessageBox db_msg;

    db_msg.setIcon(QMessageBox::Critical);
    db_msg.setWindowTitle("فشل");
    db_msg.setText("هذا الطالب موجود بالفعل");
    db_msg.setStandardButtons(QMessageBox::Close);
    db_msg.setButtonText(QMessageBox::Close,"انهاء");
    db_msg.exec();
}

void deleteSuccessfullyMsg()
{
    QMessageBox db_msg;

    db_msg.setIcon(QMessageBox::Information);
    db_msg.setWindowTitle("تمت العمليه");
    db_msg.setText("تم الازالة بنجاح");
    db_msg.setStandardButtons(QMessageBox::Close);
    db_msg.setButtonText(QMessageBox::Close,"انهاء");
    db_msg.exec();
}

void makeTableFailedMsg()
{
    QMessageBox db_msg;

    db_msg.setIcon(QMessageBox::Critical);
    db_msg.setWindowTitle("فشل");
    db_msg.setText("فشل انشاء الملف , هل هذا المجلد يسمح بانشاء الملفات؟");
    db_msg.setStandardButtons(QMessageBox::Close);
    db_msg.setButtonText(QMessageBox::Close,"انهاء");
    db_msg.exec();
}

void subjectIsNotExistMsg()
{
    QMessageBox db_msg;

    db_msg.setIcon(QMessageBox::Critical);
    db_msg.setWindowTitle("فشل");
    db_msg.setText("هذه المادة المشتركه غير موجوده");
    db_msg.setStandardButtons(QMessageBox::Close);
    db_msg.setButtonText(QMessageBox::Close,"انهاء");
    db_msg.exec();
}

void tableMakeRandomFaildMsg()
{
    QMessageBox db_msg;

    db_msg.setIcon(QMessageBox::Information);
    db_msg.setWindowTitle("فشلت العمليه");
    db_msg.setText("لم انجح في العمليه , سوف أعيد التكرار الي أن أنجح !");
    db_msg.setStandardButtons(QMessageBox::Close);
    db_msg.setButtonText(QMessageBox::Close,"انهاء");
    db_msg.exec();
}
