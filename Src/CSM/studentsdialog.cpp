#include "studentsdialog.h"
#include "ui_studentsdialog.h"
#include <QtSql>
#include "mymsg.h"
#include "configdialog.h"

studentsDialog::studentsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::studentsDialog)
{
    ui->setupUi(this);
    QSqlQuery query;
    query.exec("SELECT name FROM students");
    while(query.next())
        ui->studName->addItem(query.value(0).toString());
    query.exec(QString("SELECT name FROM subjects WHERE semester = '%1'").arg(QString::number(ConfigDialog::_semester)));
    while(query.next())
        ui->subjects->addItem(query.value(0).toString());
    query.exec("SELECT name FROM common_subjects");
    while(query.next())
        if(ui->subjects->findText(query.value(0).toString()) < 0)
            ui->subjects->addItem(query.value(0).toString());
}

studentsDialog::~studentsDialog()
{
    delete ui;
}

void studentsDialog::on_studName_currentIndexChanged(int index)
{
    ui->tableWidget->setRowCount(0);
    if(!ui->addsubject->isEnabled())
        ui->addsubject->setEnabled(true);
    QSqlQuery query;
    query.exec(QString("SELECT name FROM students_subjects WHERE id = '%1'").arg(QString::number(index+1)));
    while(query.next()){
        ui->tableWidget->setRowCount(ui->tableWidget->rowCount()+1);
        ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 0, new QTableWidgetItem(query.value(0).toString()));
    }

}

void studentsDialog::on_addsubject_clicked()
{
    for(int i=0; i<ui->tableWidget->rowCount(); i++){
        if (ui->subjects->currentText() == ui->tableWidget->item(i,0)->text()){
            subjectIsExistMsg();
            return;
        }
    }
    ui->tableWidget->setRowCount(ui->tableWidget->rowCount()+1);
    ui->tableWidget->setItem(ui->tableWidget->rowCount()-1, 0, new QTableWidgetItem(ui->subjects->currentText()));
    QSqlDatabase::database().transaction();
    QSqlQuery query;
    query.exec(QString("INSERT INTO students_subjects (id,name)"
                       "VALUES ('%1','%2')").arg(QString::number(ui->studName->currentIndex()+1),ui->subjects->currentText()));
    QSqlDatabase::database().commit();
}

void studentsDialog::on_tableWidget_itemSelectionChanged()
{
    ui->deleteSubject->setEnabled(true);
}

void studentsDialog::on_deleteSubject_clicked()
{
    QSqlQuery query;
    query.exec(QString("DELETE FROM students_subjects where id = '%1' AND name = '%2'").arg(QString::number(ui->studName->currentIndex()+1),ui->subjects->currentText()));
    ui->tableWidget->removeRow(ui->tableWidget->currentRow());
    ui->deleteSubject->setEnabled(false);
}
