#ifndef STUDENTSDIALOG_H
#define STUDENTSDIALOG_H

#include <QDialog>

namespace Ui {
class studentsDialog;
}

class studentsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit studentsDialog(QWidget *parent = 0);
    ~studentsDialog();

private slots:
    void on_studName_currentIndexChanged(int index);

    void on_addsubject_clicked();

    void on_tableWidget_itemSelectionChanged();

    void on_deleteSubject_clicked();

private:
    Ui::studentsDialog *ui;
};

#endif // STUDENTSDIALOG_H
