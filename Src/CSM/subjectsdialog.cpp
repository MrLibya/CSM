#include "subjectsdialog.h"
#include "ui_subjectsdialog.h"
#include <QtSql>
#include "mymsg.h"

SubjectsDialog::SubjectsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SubjectsDialog)
{
    ui->setupUi(this);

    ui->ClassDays->addItem("يوم");
    ui->ClassDays->addItem("يومان");
    ui->ClassDays->addItem("ثلاثة ايام");
    ui->ClassDays->addItem("أربعة ايام");
    ui->ClassDays->addItem("خمسة ايام");
    ui->ClassDays->addItem("يوميا");

    ui->ClassYear->addItem("الثانية");
    ui->ClassYear->addItem("الثالثة");
    ui->ClassYear->addItem("الرابعة");
    ui->ClassYear->addItem("الخامسة");

    ui->semester->addItem("الأول");
    ui->semester->addItem("الثاني");

    QSqlQuery query("SELECT name FROM subjects");
    while(query.next())
        ui->nClassName->addItem(query.value(0).toString());

}

SubjectsDialog::~SubjectsDialog()
{
    delete ui;
}

void SubjectsDialog::on_pushButton_clicked()
{
    if (ui->nClassName->currentText() == "" || ui->hoursClass->text() == "" || ui->professor->text() == "") {
        emptyInfoMsg();
        return;
    }
    QSqlDatabase::database().transaction();
    QSqlQuery query;
    query.exec(QString("INSERT OR REPLACE INTO subjects (id,name,professor,year,days,hours,semester)"
               "VALUES ((select id from subjects where name = '%1'),'%1','%2','%3','%4','%5','%6')").arg(ui->nClassName->currentText(),ui->professor->text(),QString::number(ui->ClassYear->currentIndex()+2),QString::number(ui->ClassDays->currentIndex()+1),ui->hoursClass->text(),QString::number(ui->semester->currentIndex()+1)));
    QSqlDatabase::database().commit();
    SubjectsDialog::close();
    addSuccessfullyMsg();
}

void SubjectsDialog::on_pushButton_3_clicked()
{
     if (yesNoMsg("هل انت متاكد؟")){
         if (ui->nClassName->findText(ui->nClassName->currentText())  < 0){
             subjectIsNotExistMsg();
             return;
         }
         deleteSuccessfullyMsg();
         QSqlQuery query;
         query.exec(QString("DELETE FROM subjects WHERE name = '%1'").arg(ui->nClassName->currentText()));
         query.exec(QString("DELETE FROM students_subjects WHERE name = '%1'").arg(ui->nClassName->currentText()));
         SubjectsDialog::close();
     }
}
