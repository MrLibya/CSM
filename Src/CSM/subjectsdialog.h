#ifndef SUBJECTSDIALOG_H
#define SUBJECTSDIALOG_H

#include <QDialog>

namespace Ui {
class SubjectsDialog;
}

class SubjectsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SubjectsDialog(QWidget *parent = 0);
    ~SubjectsDialog();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::SubjectsDialog *ui;
};

#endif // SUBJECTSDIALOG_H
